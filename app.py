from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())
import tornado.ioloop
import tornado.web
import argparse
import src.make_train_data as mtd
# import train_data as td
import json
import os
import random


# from handllers import controller as controller
class uploadFile(tornado.web.RequestHandler):
    def post(self):
        # print(self.request.body)
        file1 = self.request.files['file'][0]
        # print(file1)
        original_fname = file1['filename']
        print(original_fname)
        extension = os.path.splitext(original_fname)[1]
        print(extension)
        # fname = ''.join(random.choice(string.ascii_lowercase + string.digits) for x in range(6))
        final_filename= original_fname
        print(final_filename)
        output_file = open("data/" + final_filename, 'w')
        output_file.write(file1['body'])
        train_status_id = td.insertTrainStatus()
        response ={"status_code":200,"message":"file " + final_filename + " is uploaded successfully","train_status_id":train_status_id}
        self.set_header("Content-Type", 'application/json')
        self.set_header("Access-Control-Allow-Origin", '*')
        self.status_code =200
        self.write(json.dumps(response))
        self.finish()

class retrainSystem(tornado.web.RequestHandler):
    def get(self):
        filename = self.get_argument("filename", None, True)
        action = self.get_argument("action", None, True)
        train_status_id = self.get_argument("train_status_id", None, True)
        print(filename,action,train_status_id)
        # tagged,unttaged = mtd.extract_entities(query)
        train_status_id = td.do_everything(filename,action,train_status_id)
        # print(response)
        response ={"status_code":200,"message":"training started"}
        self.set_header("Content-Type", 'application/json')
        self.set_header("Access-Control-Allow-Origin", '*')
        self.status_code =200
        self.write(json.dumps(response))
        self.finish()
        
def make_app():
    return tornado.web.Application([
        (r"/purplle/ner/extract/v1", extract_entity_v1),
        (r"/purplle/ner/extract/v2", extract_entity),
        (r"/purplle/ner/extract_by_ean/v1", extract_entity_by_ean),
        (r"/purplle/ner/upload/v1", uploadFile),
        (r"/purplle/ner/retrain/v1", retrainSystem),
        (r"/purplle/ner/train_status/v1", train_status),
        (r"/purplle/ner/get_product_entity_tagging/v1", get_product_entity_tagging),
        (r"/purplle/ner/get_categories/v1", get_categories),
        (r"/purplle/ner/get_products_by_category/v1", get_products_by_category),
        (r"/purplle/ner/get_groups_by_category/v1", get_groups_by_category),
        (r"/images/(.*)", tornado.web.StaticFileHandler, {'path': "./data"}),
    ])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument('--cores', type=int, default=1, dest='cores')
    args = parser.parse_args()
    app = make_app()
    server = tornado.httpserver.HTTPServer(app)
    server.bind(8888)
    server.start(args.cores)  # autodetect number of cores and fork a process for each
    print("server started at port 8888")
    tornado.ioloop.IOLoop.instance().start()


    #app = make_app()
    # app.listen(8888)
    # server.bind(8888)
    # server.start(0)  # Forks multiple sub-processes
    # IOLoop.current().start()

    # tornado.ioloop.IOLoop.current().start()