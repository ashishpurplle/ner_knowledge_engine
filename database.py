import mysql.connector  
from mysql.connector import Error
import os
import re


conn=False
def get_mysql_instance (database) :
	try:
		if database == "warehouse" :
			conn = mysql.connector.connect(host=os.getenv('WH_SERVER_ADDRESS'),database=os.getenv('WH_DB_NAME'),user=os.getenv('WH_USER_NAME'),password=os.getenv('WH_PASSWORD')) 
		elif database == "slave" :
			conn = mysql.connector.connect(host=os.getenv('SERVER_ADDRESS'),database=os.getenv('DB_NAME'),user=os.getenv('USER_NAME'),password=os.getenv('PASSWORD')) 
		elif database == "sandbox" :
			conn = mysql.connector.connect(host=os.getenv('SANDBOX_SERVER_ADDRESS'),database=os.getenv('SANDBOX_DB_NAME'),user=os.getenv('SANDBOX_USER_NAME'),password=os.getenv('SANDBOX_PASSWORD'))
		elif database == "sandbox_new" :
			conn = mysql.connector.connect(host=os.getenv('SANDBOX_SERVER_ADDRESS'),database=os.getenv('SANDBOX_DB_NAME_NEW'),user=os.getenv('SANDBOX_USER_NAME'),password=os.getenv('SANDBOX_PASSWORD'))
		else:
			conn=False
		return conn
	except Error as e:
		print("not getting connection")
		print(e)
		return False	

def set_limits (sql,db):
        try:
                conn=get_mysql_instance(db)
                cursor = conn.cursor()
                cursor.execute(sql)
                return True
        except Error as e:
                print(e)
                return False

def select_data(sql,data,db) :
	try:
		conn=get_mysql_instance(db)
		cursor = conn.cursor()
		cursor.execute("SET SESSION group_concat_max_len = 10000000;")
		cursor.execute(sql,data)
		rows = cursor.fetchall()
		return rows
	except Error as e:
		print(e)
		return []

def fetch_data(sql,db) :
        try:
                conn=get_mysql_instance(db)
                cursor = conn.cursor()
                cursor.execute(sql)
                rows = cursor.fetchall()
                return rows
        except Error as e:
                print(e)
                return []

def update_data(sql,data,db) :
	try :
		conn=get_mysql_instance(db)
		cursor=conn.cursor()
		cursor.execute(sql,data)
		conn.commit()
		return cursor.rowcount
	except Error as e:
		print(e)
		return False	

def insert_data(sql,db):
	try :
		conn=get_mysql_instance(db)
		cursor=conn.cursor()
		cursor.execute(sql)
		conn.commit()
		return cursor.rowcount
	except Error as e:
		print(e)
		return False

def truncate_table(table_name,db):
	try :
		conn=get_mysql_instance(db)
		cursor=conn.cursor()
		cursor.execute("truncate table "+table_name)
		conn.commit()
		return True
	except Error as e:
		print(e)
		return False

def get_content_from_database(source_info) :
	doc_text=""
	success_flag=False
	conn=get_mysql_instance("slave")
	cursor = conn.cursor()
	sql = "select "+source_info[3]+" from  product_product WHERE id="+str(source_info[6])
	cursor.execute(sql)
	row = cursor.fetchone()	
	if len(row) > 0 :
		if row[0] == None :
			doc_text=""
		else :
			doc_text=row[0]
			success_flag=True
	doc_text=re.sub('<[A-Za-z\/][^>]*>', '', doc_text)
	
	if source_info[3] == "features" : 
		doc_text=doc_text.replace('\n', '.').replace('\r', '').replace('\t','').strip()
	doc_text=doc_text.replace('\n', '').replace('\r', '').replace('\t','').strip()
	target=open(os.environ['KNOWLEDGE_ENGINE']+'data/raw','w+')
	target.write(doc_text)
	target.close
	return success_flag
 
if __name__ == '__main__':       # when run as a script
	print(get_mysql_instance("slave"))

