import pandas as pd
import numpy as np
import os,sys
sys.path.append('/Users/ashish/purplle-workspace/ner_knowledge_engine/')
# print(sys.path)
from os.path import join, dirname
from dotenv import load_dotenv
# print(dirname(__file__))
dotenv_path = join(sys.path[-1], '.env')
load_dotenv(dotenv_path)

from google.cloud import vision
from google.cloud.vision import types
from sqlalchemy import create_engine

# print(sys.path)
import database as SQL

engine = create_engine("mysql+pymysql://"+os.getenv('SANDBOX_USER_NAME')+":"+os.getenv('SANDBOX_PASSWORD')+"@"+os.getenv('SANDBOX_SERVER_ADDRESS')+"/"+os.getenv('SANDBOX_DB_NAME'))
con = engine.connect()

def extract_entities_from_image(source):
	client = vision.ImageAnnotatorClient()
	if source == "purplle":
		conn= SQL.get_mysql_instance('slave')
		sql ="select p.ean_code,concat('https://media6.ppl-media.com/static/img/product/',p.id,'/',img.name,'.',img.ext) as image_description,num,pc.category_id from product_product p join product_productcategory pc on p.id = pc.product_id join `ext_imagedetail` img on img.module_id = p.id where p.active =1 and img.module='product' and img.size ='base' and img.is_active =1 and num in(1,2) and pc.category_id in(2,3,4,5,6,7,8,9,10,11,12,13,14,82,91,158,169,188,190,235,299,300,344,381,26,27,28,29,31,90,133,170,204,215,216,218,237,250,252,305,315,316,348,371,217,219,233,244,292,311) order by p.`ean_code`";
	if source == "nykaa":
		conn= SQL.get_mysql_instance('sandbox')
		sql ="select a.product_ean as ean_code,c.image_url as image_description,a.internal_category_id as category_id from master_product a join `master_source_product_tagging` b on a.id = b.master_product_id join `master_product_image_urls` c on c.`source_product_id` = b.id where c.num =1  and a.internal_category_id in(2,3,4,5,6,7,8,9,10,11,12,13,14,82,91,158,169,188,190,235,299,300,344,381,26,27,28,29,31,90,133,170,204,215,216,218,237,250,252,305,315,316,348,371,217,219,233,244,292,311)"
	df=pd.read_sql(sql,conn)
	print(df.describe())
	print("total->",len(df))
	c =0
	try:
		for index,row in df.iterrows():
			if index > 2902:
				df_image_description = pd.DataFrame({"ean_code":[],"image_description":[],"category_id":[],"source":[]})
				print("processing for image -> "+row["ean_code"]+"-<<<<",str(index)+"/"+str(len(df)))
				file_name = row['image_description']
				response = client.annotate_image({
				  'image': {'source': {'image_uri': file_name}},
				  'features': [{'type': vision.enums.Feature.Type.TEXT_DETECTION}],
				})
				texts = response.text_annotations
				if(len(texts)> 0):
					str_text = texts[0].description.lower()
					str_text = str_text.replace('\n','.').lower()
					df_image_description = df_image_description.append({"ean_code":row["ean_code"],"image_description":str_text,"category_id":row["category_id"],"source":source},ignore_index=True)
					df_image_description.to_sql(con=con, name='NER_product_image_description', if_exists='append',index=False)
		
	except Exception as e:
		print("something went wrong for ",e)

if __name__ == '__main__':
	extract_entities_from_image('purplle')
	extract_entities_from_image('nykaa')
