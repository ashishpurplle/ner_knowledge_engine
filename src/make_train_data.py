import pandas as pd
import numpy as np
import os,sys
import re
# sys.path.append('/Users/ashish/purplle-workspace/ner_knowledge_engine/')
sys.path.append('/home/ashish/ner_knowledge_engine/')

from os.path import join, dirname
from dotenv import load_dotenv
dotenv_path = join(dirname(sys.path[-1]), '.env')
load_dotenv(dotenv_path)
import database as SQL
from google.cloud import vision
from google.cloud.vision import types
from sqlalchemy import create_engine
import nltk
from nltk import word_tokenize

engine = create_engine("mysql+pymysql://"+os.getenv('SANDBOX_USER_NAME')+":"+os.getenv('SANDBOX_PASSWORD')+"@"+os.getenv('SANDBOX_SERVER_ADDRESS')+"/"+os.getenv('SANDBOX_DB_NAME'))
con = engine.connect()

def build_ner_train_data_sentences(from_db,source,ean_code="",category_id=""):
	from bs4 import BeautifulSoup
	print("-------->Building Training Data<--------")
	print(from_db,source)
	if from_db == 'True':
		if source == 'purplle':
			conn= SQL.get_mysql_instance('slave')
			if(category_id == "all"):
				sql ="select p.ean_code,pc.category_id,p.name as product_name,p.description,p.short_description,p.features from product_product p left join product_productcategory pc on pc.product_id = p.id where p.active =1  and pc.category_id in(2,3,4,5,6,7,8,9,10,11,12,13,14,82,91,158,169,188,190,235,299,300,344,381,26,27,28,29,31,90,133,170,204,215,216,218,237,250,252,305,315,316,348,371,217,219,233,244,292,311)"
			else:
				sql ="select p.ean_code,pc.category_id,p.name as product_name,p.description,p.short_description,p.features,i.image_description from product_product p left join product_productcategory pc on pc.product_id = p.id left join `NER_product_image_description` i on i.ean_code=p.ean_code where p.active =1  and pc.category_id in("+category_id+")"
			if(ean_code):
				sql ="select p.ean_code,pc.category_id,p.name as product_name,p.description,p.short_description,p.features,i.image_description from product_product p left join product_productcategory pc on pc.product_id = p.id left join `NER_product_image_description` i on i.ean_code=p.ean_code where p.active =1  and p.ean_code in('"+ean_code+"')"
		if source == 'image':
			conn= SQL.get_mysql_instance('sandbox')
			if(category_id == "all"):
				sql ="select i.ean_code,pc.category_id,i.image_description from NER_product_image_description i left JOIN product_product p on i.ean_code=p.ean_code left join product_productcategory pc on pc.product_id = p.id where p.active =1  and pc.category_id in(2,3,4,5,6,7,8,9,10,11,12,13,14,82,91,158,169,188,190,235,299,300,344,381,26,27,28,29,31,90,133,170,204,215,216,218,237,250,252,305,315,316,348,371,217,219,233,244,292,311) and i.source='purplle' GROUP BY i.ean_code,i.image_description"
			else:
				sql ="select p.ean_code,pc.category_id,p.name as product_name,p.description,p.short_description,p.features,i.image_description from product_product p left join product_productcategory pc on pc.product_id = p.id left join `NER_product_image_description` i on i.ean_code=p.ean_code where p.active =1  and pc.category_id in("+category_id+")"
			if(ean_code):
				sql ="select p.ean_code,pc.category_id,p.name as product_name,p.description,p.short_description,p.features,i.image_description from product_product p left join product_productcategory pc on pc.product_id = p.id left join `NER_product_image_description` i on i.ean_code=p.ean_code where p.active =1  and p.ean_code in('"+ean_code+"')"
		
		if source == 'nykaa':
			conn= SQL.get_mysql_instance('sandbox')
			if(category_id == "all"):
				sql ="select distinct product_ean as ean_code,product_name,msp.`source_product_description` as description,msp.`source_product_features` as features,`internal_category_id` as category_id,i.`image_description` from `master_product` mp left join master_source_product_tagging msp on mp.`id` = msp.`master_product_id` left join `product_image_description` i on i.ean_code=mp.product_ean  where `internal_category_id` in(2,3,4,5,6,7,8,9,10,11,12,13,14,82,91,158,169,188,190,235,299,300,344,381,26,27,28,29,31,90,133,170,204,215,216,218,237,250,252,305,315,316,348,371,217,219,233,244,292,311)"
			else:
				sql ="select distinct product_ean as ean_code,product_name,msp.`source_product_description` as description,msp.`source_product_features` as features,`internal_category_id` as category_id,i.`image_description` from `master_product` mp left join master_source_product_tagging msp on mp.`id` = msp.`master_product_id` left join `product_image_description` i on i.ean_code=mp.product_ean  where `internal_category_id` in("+category_id+")"
			if(ean_code):
				sql ="select distinct product_ean as ean_code,product_name,msp.`source_product_description` as description,msp.`source_product_features` as features,`internal_category_id` as category_id,i.`image_description` from `master_product` mp left join master_source_product_tagging msp on mp.`id` = msp.`master_product_id` left join `product_image_description` i on i.ean_code=mp.product_ean  where mp.product_ean in('"+ean_code+"'')"
		df=pd.read_sql(sql,conn)
		print("-------->Reading Training Data for column<--------......."+str(len(df)))
		# print(df)
		columns = list(df.columns.values)
		columns = list(set(columns)^set(["ean_code","category_id"]))
		conn= SQL.get_mysql_instance('sandbox')
		for index,row in df.iterrows():
			print("processing......"+str(index)+"-<<<<<",len(df))
			df_pos = pd.DataFrame({"ean_code":[],"text":[],"source_column":[],"category_id":[],"source":[]})
			try:
				for column in columns:
					print("processing......"+str(index)+"-<<<<<"+column,len(df))
					if row[column] != "nan" and str(row[column]) != "None":
						text = row[column]
						text=text.encode('utf-8').strip()
						cleantext = BeautifulSoup(text, "lxml").text
						sentences = nltk.sent_tokenize(cleantext) 
						if len(sentences) > 0:
							for sentence in sentences:
								df_pos = df_pos.append({"ean_code":row["ean_code"],"text":sentence,"source_column":column,"category_id":row["category_id"],"source":source},ignore_index=True)		
				print(len(df_pos))
				if(len(df_pos) > 0):
					df_pos.to_sql(con=con, name='NER_train_sentences', if_exists='append',index=False)
			except Exception as e:
				print("Exception in text -> ", e)
		df_pos.to_csv(os.path.join(os.environ.get("BASE_PATH"),"data/NER_sentences.csv"),index=None)
	else:
		df_pos = pd.read_csv(os.path.join(os.environ.get("BASE_PATH"),"data/NER_train_sentences.csv"))

	print("<-Total sentences to process->",len(df_pos))

def remove_special_characters(text):
	"""remove characters that are not indicators of phrase boundaries"""
	return re.sub("([{}@\"$%&\\\/*'\"]|\d)", "", text)

def generate_candidate_phrases(text, stopwords):
	char_splitter = re.compile("[.,;!:()-]")
	""" generate phrases using phrase boundary markers """

	# generate approximate phrases with punctation
	coarse_candidates = char_splitter.split(text.lower())

	candidate_phrases = []

	for coarse_phrase\
			in coarse_candidates:

		words = re.split("\\s+", coarse_phrase)
		previous_stop = False

		# examine each word to determine if it is a phrase boundary marker or
		# part of a phrase or lone ranger
		for w in words:

			if w in stopwords and not previous_stop:
				# phrase boundary encountered, so put a hard indicator
				candidate_phrases.append(";")
				previous_stop = True
			elif w not in stopwords and len(w) > 0:
				# keep adding words to list until a phrase boundary is detected
				candidate_phrases.append(w.strip())
				previous_stop = False

	# get a list of candidate phrases without boundary demarcation
	phrases = re.split(";+", ' '.join(candidate_phrases))

	# return phrases
	return coarse_candidates

def create_phrases(category_id=""):
	conn= SQL.get_mysql_instance('sandbox')
	# sql ="select * from `NER_sentences` where category_id in(2,3,4,5,6,7,8,9,10,11,12,13,14,82,91,158,169,188,190,235,299,300,344,381,26,27,28,29,31,90,133,170,204,215,216,218,237,250,252,305,315,316,348,371,217,219,233,244,292,311) and source ='nykaa' and source_column='image_description' "
	category_id =2,3,4,5,6,7,8,9,10,11,12,13,14,82,91,158,169,188,190,235,299,300,344,381,26,27,28,29,31,90,133,170,204,215,216,218,237,250,252,305,315,316,348,371,217,219,233,244,292,311
	sql ="select * from `NER_train_sentences` where category_id in "+str(category_id)
	df = pd.read_sql(sql,conn)
	print(len(df))
	from nltk.corpus import stopwords 
	stopwords = set(stopwords.words('english'))
	
	for index,row in df.iterrows():
		df_f = pd.DataFrame({"sentence_id":[],"ean_code":[],"source_column":[],"category_id":[],"source":[]})
		text = row["text"]
		print(row['id'],len(df))
		if(text != ""):
			text = remove_special_characters(text)
			phrases = generate_candidate_phrases(text,stopwords)
			for phrase in phrases:
				if phrase:
					df_f = df_f.append({'sentence_id':row['id'],"ean_code":row["ean_code"],"phrase":phrase,"source_column":row["source_column"],"category_id":row["category_id"],"source":row["source"]},ignore_index=True)
			df_f.to_sql(con=con, name='NER_train_phrases', if_exists='append',index=False)


def make_train_data(category_id=""):
	conn= SQL.get_mysql_instance('sandbox')
	category_id =2,3,4,5,6,7,8,9,10,11,12,13,14,82,91,158,169,188,190,235,299,300,344,381,26,27,28,29,31,90,133,170,204,215,216,218,237,250,252,305,315,316,348,371,217,219,233,244,292,311
	sql ="select * from NER_train_phrases where category_id in "+str(category_id)
	df=pd.read_sql(sql,conn)
	print("******Entity Extraction started for dataset of length******",len(df))
	docs =[]
	sentance_count =0
	
	for index,row in df.iterrows():
		try:
			df_f = pd.DataFrame({"phrase_id":[],"Word":[],"Tag":[],"category_id":[],"source_column":[],"ean_code":[],'source':[]})
			print(index,len(df))
			main_text = row["phrase"]
			cleanString = re.sub('\W+',' ',main_text)
			text = cleanString.lower()
			words = word_tokenize(text)
			for w in words:
				df_f = df_f.append({"ean_code":row["ean_code"],"phrase_id":row["id"],"Word":w,"Tag":"","category_id":row["category_id"],"source_column":row["source_column"],'source':row['source']},ignore_index=True)
		except Exception as e:
			print("Exception in text -> ", e)	
		df_f.to_sql(con=con, name='NER_train_data', if_exists='append',index=False)


def retrain_train_data(train_status_id="",category_id="",source=""):
	if(train_status_id):
		return update_train_status(train_status_id,"tag_product_entity ")
	conn= SQL.get_mysql_instance('sandbox')
	if(category_id == "skin"):
		category_id =2,3,4,5,6,7,8,9,10,11,12,13,14,82,91,158,169,188,190,235,299,300,344,381
		entity_group_arr = 19,21,59,77,43
	if(category_id == "hair"):
		category_id =26,27,28,29,31,90,133,170,204,215,216,218,237,250,252,305,315,316,348,371,217,219,233,244,292,311
		entity_group_arr = 9,12,59,77,43
	
	final_child_cat_ids = category_id

	if(source == "all"):
		sql ="select Lower(eg.name) as `group`,Lower(ev.entity_value) as entity,ev.entity_group_id,ev.id as entity_id from NER_entity_groups eg join NER_entity_values ev on ev.entity_group_id =eg.id where eg.id != 46 and eg.id in "+str(entity_group_arr)+" group by ev.`entity_group_id`,ev.id"
	if(source == "alias"):
		sql ="select Lower(eg.name) as `group`,Lower(ev.entity_value) as entity,ev.entity_group_id,ev.id as entity_id from NER_entity_groups eg join NER_entity_values ev on ev.entity_group_id =eg.id JOIN entity_aliases ea on ea.module_id = ev.id  where ea.module='entity_value' and eg.id != 46 and eg.id in "+str(entity_group_arr)+" group by ev.`entity_group_id`,ev.id"
	df=pd.read_sql(sql,conn)
	print("total Size->>>",len(df))
	df["entity"] = df["entity"].str.replace("&","and")
	df["arr_entity"] = df["entity"].str.split(" ")
	df["arr_entity_len"] = df["arr_entity"].str.len()
	max_ent_len = df["arr_entity_len"].max()
	for i in range(1,5):
		df_len = df[df["arr_entity_len"] == i]
		print(len(df_len))
		print("Processing For length->",i)
		for index,row in df_len.iterrows():
			entity = row["entity"].lower().split()
			group = row["group"].lower()
			entity_id = row["entity_id"]
			entity_group_id = row["entity_group_id"]
			if(i == 1):
				print(entity,group)
				sql1 = "UPDATE NER_train_data set Tag = 'B-"+group+"',entity_id="+str(entity_id)+",entity_group_id="+str(entity_group_id)+" where Word = '"+entity[0]+"' and category_id in "+str(final_child_cat_ids)
				cursor=conn.cursor()
				cursor.execute(sql1)
				conn.commit()
				print(cursor.rowcount)
			if(i == 2):
				print(entity,group)
				sql1="SELECT g1.id as id1,g2.id as id2 FROM NER_train_data g1 INNER JOIN NER_train_data g2 ON g2.id = g1.id + 1 WHERE g1.Word = '"+entity[0]+"' and g2.Word = '"+entity[1]+"' and g1.category_id in "+str(final_child_cat_ids)
				cursor=conn.cursor()
				cursor.execute(sql1)
				rows = cursor.fetchall()
				for row in rows:
					sql_a = "UPDATE NER_train_data set Tag = 'B-"+group+"',entity_id="+str(entity_id)+",entity_group_id="+str(entity_group_id)+" WHERE id ="+str(row[0])
					sql_b = "UPDATE NER_train_data set Tag = 'I-"+group+"',entity_id="+str(entity_id)+",entity_group_id="+str(entity_group_id)+" WHERE id ="+str(row[1])
					cursor.execute(sql_a)
					conn.commit()
					cursor.execute(sql_b)
					conn.commit()
			if(len(entity) == 3):
				print(entity,group)
				sql1="SELECT g1.id as id1,g2.id as id2,g3.id as id3 FROM NER_train_data g1 INNER JOIN NER_train_data g2 ON g2.id = g1.id + 1 INNER JOIN NER_train_data g3 ON g3.id = g1.id + 2 WHERE g1.Word = '"+entity[0]+"' and g2.Word = '"+entity[1]+"' and g3.Word='"+entity[2]+"' and g1.category_id in "+str(final_child_cat_ids)
				cursor=conn.cursor()
				cursor.execute(sql1)
				rows = cursor.fetchall()
				for row in rows:
					sql_a = "UPDATE NER_train_data set Tag = 'B-"+group+"',entity_id="+str(entity_id)+",entity_group_id="+str(entity_group_id)+" WHERE id ="+str(row[0])
					sql_b = "UPDATE NER_train_data set Tag = 'I-"+group+"',entity_id="+str(entity_id)+",entity_group_id="+str(entity_group_id)+" WHERE id ="+str(row[1])
					sql_c = "UPDATE NER_train_data set Tag = 'I-"+group+"',entity_id="+str(entity_id)+",entity_group_id="+str(entity_group_id)+" WHERE id ="+str(row[2])
					cursor.execute(sql_a)
					conn.commit()
					cursor.execute(sql_b)
					conn.commit()
					cursor.execute(sql_c)
			if(len(entity) == 4):
				print(entity,group)
				sql1="SELECT g1.id as id1,g2.id as id2,g3.id as id3,g4.id as id4 FROM NER_train_data g1 INNER JOIN NER_train_data g2 ON g2.id = g1.id + 1 INNER JOIN NER_train_data g3 ON g3.id = g1.id + 2 INNER JOIN NER_train_data g4 ON g4.id = g1.id + 3 WHERE g1.Word = '"+entity[0]+"' and g2.Word = '"+entity[1]+"' and g3.Word='"+entity[2]+"' and g4.Word='"+entity[3]+"' and g1.category_id in "+str(final_child_cat_ids)
				cursor=conn.cursor()
				cursor.execute(sql1)
				rows = cursor.fetchall()
				for row in rows:
					sql_a = "UPDATE NER_train_data set Tag = 'B-"+group+"',entity_id="+str(entity_id)+",entity_group_id="+str(entity_group_id)+" WHERE id ="+str(row[0])
					sql_b = "UPDATE NER_train_data set Tag = 'I-"+group+"',entity_id="+str(entity_id)+",entity_group_id="+str(entity_group_id)+" WHERE id ="+str(row[1])
					sql_c = "UPDATE NER_train_data set Tag = 'I-"+group+"',entity_id="+str(entity_id)+",entity_group_id="+str(entity_group_id)+" WHERE id ="+str(row[2])
					sql_d = "UPDATE NER_train_data set Tag = 'I-"+group+"',entity_id="+str(entity_id)+",entity_group_id="+str(entity_group_id)+" WHERE id ="+str(row[3])
					cursor.execute(sql_a)
					conn.commit()
					cursor.execute(sql_b)
					conn.commit()
					cursor.execute(sql_c)
					conn.commit()
					cursor.execute(sql_d)
					conn.commit()
					print(cursor.rowcount)

def tag_product_entity(train_status_id="",category_id=""):
	if(train_status_id):
		return update_train_status(train_status_id,"tag_product_entity ")
	conn= SQL.get_mysql_instance('sandbox')
	cursor=conn.cursor()
	category_id =2,3,4,5,6,7,8,9,10,11,12,13,14,82,91,158,169,188,190,235,299,300,344,381,26,27,28,29,31,90,133,170,204,215,216,218,237,250,252,305,315,316,348,371,217,219,233,244,292,311
	sql="update NER_train_data set Tag_new = SUBSTRING(Tag, 3) where Tag != 'O' and category_id in "+str(category_id)
	cursor.execute(sql)
	conn.commit()
	sql_1 ="update NER_train_data set Tag='O' WHERE Tag ='' and category_id in "+str(category_id)
	cursor.execute(sql_1)
	conn.commit()
	sql_2 ="update NER_train_data set Tag_new='O' WHERE Tag = 'O' and category_id in"+str(category_id)
	cursor.execute(sql_2)
	conn.commit()
	# sql1 ="truncate table NER_entity_product_new"
	sql2 ="truncate table NER_entity_product_by_source"
	sql3 ="truncate table NER_entity_product_scores_by_source"
	# cursor.execute(sql1)
	# conn.commit()
	cursor.execute(sql2)
	conn.commit()
	cursor.execute(sql3)
	conn.commit()
	print("NER_train_data Tag_new Updated",cursor.rowcount)
	sql="select * from `NER_train_data` where Tag!= 'O' and category_id in "+str(category_id)
	df=pd.read_sql(sql,conn)
	
	df_len = len(df)
	unique_ean_codes = df["ean_code"].unique()
	c =0
	for ean_code in unique_ean_codes:
		c = c+1
		print(c,len(unique_ean_codes))
		df_f = pd.DataFrame({"ean_code":[],"entity_id":[],"entity_value":[],"entity_group_id":[],"group_name":[],"source_column":[]})
		df_ean = df[df["ean_code"] == ean_code]
		for index,row in df_ean.iterrows():
			if "B-" in df.iloc[index]["Tag"]:
				entity = df.iloc[index]["Word"]
				if index+1 < df_len and "I-" in df.iloc[index+1]["Tag"]:
					entity = entity+ " "+df.iloc[index+1]["Word"]
					if index+2 < df_len and  "I-" in df.iloc[index+2]["Tag"]:
						entity = entity+ " "+df.iloc[index+2]["Word"]
						if index+3 < df_len and "I-" in df.iloc[index+3]["Tag"]:
							entity = entity+ " "+df.iloc[index+3]["Word"]
						else:
							df_f = df_f.append({"ean_code":row["ean_code"],"entity_id":row["entity_id"],"entity_value":entity,"entity_group_id":row["entity_group_id"],"group_name":row["Tag_new"],"source_column":row["source_column"]},ignore_index=True)
					else:
						df_f = df_f.append({"ean_code":row["ean_code"],"entity_id":row["entity_id"],"entity_value":entity,"entity_group_id":row["entity_group_id"],"group_name":row["Tag_new"],"source_column":row["source_column"]},ignore_index=True)
				else:
					df_f = df_f.append({"ean_code":row["ean_code"],"entity_id":row["entity_id"],"entity_value":entity,"entity_group_id":row["entity_group_id"],"group_name":row["Tag_new"],"source_column":row["source_column"]},ignore_index=True)
			# print(df.iloc[index]["Tag"])
		# print(df_f)
		df_f.to_sql(con=con, name='NER_entity_product_by_source', if_exists='append',index=False)
	conn= SQL.get_mysql_instance('sandbox')
	cursor=conn.cursor()
	sql ="UPDATE NER_entity_product_by_source a INNER JOIN `NER_entity_values` b ON a.entity_id = b.id SET a.entity_value = b.entity_value"
	cursor.execute(sql)
	conn.commit()
	print("Entity Values Updated",cursor.rowcount)
	sql ="insert into NER_entity_product_scores_by_source(ean_code,entity_id,entity_value,entity_group_id,group_name,source_column,score) (select ean_code,entity_id,entity_value,entity_group_id,group_name,source_column,count(*) as score from NER_entity_product_by_source group by ean_code,entity_id,entity_group_id,source_column)"
	cursor.execute(sql)
	conn.commit()
	print("NER_entity_product_1 created",cursor.rowcount)
	sql_1 ="update NER_entity_product_scores_by_source set score = score * 5 where source_column in('product_name','image_description')"
	sql_2 ="update NER_entity_product_scores_by_source set score = score * 4 where source_column in('short_description','features')"
	sql_3 ="update NER_entity_product_scores_by_source set score = score * 3 where source_column in('description','alt_description')"
	cursor.execute(sql_1)
	conn.commit()
	cursor.execute(sql_2)
	conn.commit()
	cursor.execute(sql_3)
	conn.commit()
	print("NER_entity_product_1 Scores updated")
	sql_final = "insert into NER_entity_product_final(ean_code,entity_id,entity_value,entity_group_id,group_name,score) (select ean_code,entity_id,entity_value,entity_group_id,group_name,sum(score) as score from NER_entity_product_scores_by_source group by ean_code,entity_id,entity_group_id)"
	cursor.execute(sql_final)
	conn.commit()
	print("done")

if __name__ == '__main__':
	# from_db = sys.argv[1]
	# source = sys.argv[2]
	# ean_code = sys.argv[3]
	# category_id = sys.argv[4]
	# build_ner_train_data_sentences("True",'purplle',"","all")
	# build_ner_train_data_sentences("True",'image',"","all")
	# build_ner_train_data_sentences("True",'nykaa',"","all")
	# create_phrases("")
	# make_train_data("")
	retrain_train_data("","skin","all")
	retrain_train_data("","skin","alias")
	retrain_train_data("","hair","all")
	retrain_train_data("","hair","alias")
	tag_product_entity("","")
