def eval(is_train,from_db):
	if is_train:
		if from_db:
			conn= SQL.get_mysql_instance('sandbox')
			# sql = "select * from ner_train_dataset_new where Tag in ('B-active ingredients','I-active ingredients','B-benefits','I-benefits','B-skin concern','I-skin concern','B-hair concern','I-hair concern','B-skin type','I-skin type','B-hair type','I-hair type')"
			# sql ="select * from NER_train_data WHERE category_id in(2,3,4,5,6,7,8,9,10,11,12,13,14,82,91,158,169,188,190,235,299,300,344,381)"
			# sql ="SELECT * FROM NER_train_data WHERE Tag != 'O'"
			sql="SELECT * FROM NER_train_data WHERE phrase_id in(SELECT distinct phrase_id FROM NER_train_data WHERE Tag !='O')"
			data=pd.read_sql(sql,conn)
			data.to_csv("data/ner_train_dataset_new.csv",index=None)
		else:
			data = pd.read_csv("data/ner_train_dataset_new.csv")
	
		print(data.describe())
		print(data.head(10))
		data = data.fillna(method="ffill")
		print("Number of sentences: ", len(data.groupby(['phrase_id'])))
		words = list(set(data["Word"].values))
		n_words = len(words)
		print("TOTAL NO. of Words",n_words)

		tags = list(set(data["Tag"].values))
		n_tags = len(tags)
		print("TOTAL NO. of Tags",n_tags)

		getter = SentenceGetter(data)
		sentences = getter.sentences
		# MAX_LEN = max([len(s) for s in sentences])
		print("MAX_LEN",MAX_LEN)
		word2idx = {w: i + 2 for i, w in enumerate(words)}
		word2idx["UNK"] = 1 # Unknown words
		word2idx["PAD"] = 0 # Padding
		# print("**********word2idx**********",word2idx)
		idx2word = {i: w for w, i in word2idx.items()}
		# print("**********idx2word*********",idx2word)
		tag2idx = {t: i+1 for i, t in enumerate(tags)}
		tag2idx["PAD"] = 0
		# print("**********tag2idx*********",tag2idx)
		idx2tag = {i: w for w, i in tag2idx.items()}
		# print("**********idx2tag*********",idx2tag)
		# Convert each sentence from list of Token to list of word_index
		X = [[word2idx[w[0]] for w in s] for s in sentences]
		X = pad_sequences(maxlen=MAX_LEN, sequences=X, padding="post", value=word2idx["PAD"])

		# Convert Tag/Label to tag_index
		y = [[tag2idx[w[1]] for w in s] for s in sentences]
		# Padding each sentence to have the same lenght
		y = pad_sequences(maxlen=MAX_LEN, sequences=y, padding="post", value=tag2idx["PAD"])
		# One-Hot encode
		y = [to_categorical(i, num_classes=n_tags+1) for i in y]  # n_tags+1(PAD)
		
		X_tr, X_te, y_tr, y_te = train_test_split(X, y, test_size=0.1)
		
		print(X_tr.shape, X_te.shape) 
		print(np.array(y_tr).shape, np.array(y_te).shape)

		# print('After processing, sample:', X[0])
		# print('After processing, labels:', y[0])

		# Model definition
		input = Input(shape=(MAX_LEN,))
		model = Embedding(input_dim=n_words+2, output_dim=EMBEDDING, # n_words + 2 (PAD & UNK)
						  input_length=MAX_LEN, mask_zero=True)(input)  # default: 20-dim embedding
		model = Bidirectional(LSTM(units=50, return_sequences=True,recurrent_dropout=0.1))(model)  # variational biLSTM
		model = TimeDistributed(Dense(50, activation="sigmoid"))(model)  # a dense layer as suggested by neuralNer
		crf = CRF(n_tags+1)  # CRF layer, n_tags+1(PAD)
		out = crf(model)  # output

		model = Model(input, out)
		model.compile(optimizer="adam", loss=crf.loss_function, metrics=[crf.accuracy])
		# model.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"])

		model.summary()

		history = model.fit(X_tr, np.array(y_tr), batch_size=BATCH_SIZE, epochs=EPOCHS, verbose=1)
		# save the model to disk
		filename = 'model_new.h5'
		model.save(filename)
		pred_cat = model.predict(X_te)
		pred = np.argmax(pred_cat, axis=-1)
		y_te_true = np.argmax(y_te, -1)
		# Convert the index to tag
		pred_tag = [[idx2tag[i] for i in row] for row in pred]
		y_te_true_tag = [[idx2tag[i] for i in row] for row in y_te_true] 

		report = flat_classification_report(y_pred=pred_tag, y_true=y_te_true_tag)
		print(report)
		print("Training Done And saved in Model")
		for i in range(0,100):
			p = model.predict(np.array([X_te[i]]))
			p = np.argmax(p, axis=-1)
			true = np.argmax(y_te[i], -1)

			print("Sample number {} of {} (Test Set)".format(i, X_te.shape[0]))
			# Visualization
			print("{:15}||{:5}||{}".format("Word", "True", "Pred"))
			print(30 * "=")
			for w, t, pred in zip(X_te[i], true, p[0]):
				if w != 0:
					print("{:15}: {:5} {}".format(words[w-2], idx2tag[t], idx2tag[pred]))
	else:
		df_pos = pd.DataFrame({"text":["Cardamom, black tea, geranium and musk are in the heart, surrounded by refreshing marine tones. The base is made of sandalwood, cedar, moss, tonka, vetiver and musk. Dominique Preyssas is the nose behind the composition"]})
		data = extract_entity(df_pos,True)
		test_ner(data)